from turtle import Turtle

MOVE = 10
MOVE_SPEED = 0.09


class Ball(Turtle):

    def __init__(self):
        super().__init__()
        self.shape('circle')
        self.color('aqua')
        self.penup()
        self.x_move = MOVE
        self.y_move = MOVE
        self.move_speed = MOVE_SPEED

    def move(self):
        new_x = self.xcor() + self.x_move
        new_y = self.ycor() + self.y_move
        self.goto(new_x, new_y)

    def bounce_y(self):
        self.y_move *= -1

    def bounce_x(self):
        self.x_move *= -1
        self.move_speed *= 0.7

    def reset_position(self):
        self.move_speed = MOVE_SPEED
        self.goto(0, 0)
        self.bounce_x()
