from turtle import Screen
from paddle import Paddle
from ball import Ball
from scoreboard import Scoreboard
import time

X_POS = 350
COLLISION_Y_BALL = 280
BALL_DISTANCE = 50
BALL_WALL = 320

# Screen:
screen = Screen()
screen.setup(width=800, height=600, starty=350)
screen.bgcolor('black')
screen.title('Pong')
screen.tracer(0)

# Turtle:
l_paddle = Paddle(-X_POS)
r_paddle = Paddle(X_POS)

# Scoreboard:
sc = Scoreboard()

# Ball:
ball = Ball()

screen.listen()
# left:
screen.onkey(l_paddle.go_up, 'Up')
screen.onkey(l_paddle.go_down, 'Down')
# right:
screen.onkey(r_paddle.go_up, 'w')
screen.onkey(r_paddle.go_down, 's')

game_is_on = True

while game_is_on:
    time.sleep(ball.move_speed)
    screen.update()
    ball.move()

    # Detect collision with wall:
    if ball.ycor() > COLLISION_Y_BALL or ball.ycor() < - COLLISION_Y_BALL:
        ball.bounce_y()

    # Detect collision with paddle:
    if (ball.distance(r_paddle) < BALL_DISTANCE and ball.xcor() > BALL_WALL) or \
            (ball.distance(l_paddle) < BALL_DISTANCE and ball.xcor() < -BALL_WALL):
        ball.bounce_x()

    # Detect R paddle misses:
    if ball.xcor() > BALL_WALL + 60:
        ball.reset_position()
        sc.l_point()

    # Detect L paddle misses:
    if ball.xcor() < - (BALL_WALL + 60):
        ball.reset_position()
        sc.r_point()

screen.exitonclick()
