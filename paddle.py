from turtle import Turtle

Y_POSITION = 20


class Paddle(Turtle):

    def __init__(self, x_cor):
        super().__init__()
        self.x_cor = x_cor
        self.create_self(self.x_cor)

    def create_self(self, set_x):
        self.shape('square')
        self.color('white')
        self.turtlesize(5, 1)
        self.penup()
        self.goto(set_x, 0)

    def go_up(self):
        new_y = self.ycor() + Y_POSITION
        self.goto(self.xcor(), new_y)

    def go_down(self):
        new_y = self.ycor() - Y_POSITION
        self.goto(self.xcor(), new_y)
